// console.log("Hello World");

function printSum(sum1,sum2){
	let sum = sum1 + sum2;
	console.log("Displayed the sum of " + sum1 + " and " + sum2);
	console.log(sum);
};
printSum(5,15);

function printDifference(diff1,diff2){
	let difference = diff1 - diff2;
	console.log("Displayed the difference of " + diff1 + " and " + diff2);
	console.log(difference);
};
printDifference(20,5);

function printProduct(num1,num2){
	console.log("The product of " + num1 + " and " + num2 + ":");
	let product = num1 * num2;
	return product;
};
let product = printProduct(50,10);
console.log(product);


function printQuotient(num1,num2){
	console.log("The quotient of " + num1 + " and " + num2 + ":");
	let quotient = num1 / num2;
	return quotient;
};
let quotient = printQuotient(50,10);
console.log(quotient);

function printCircleArea(area){
	console.log("The result of getting the area of a circle with " + area + " radius:");
	let circleArea =(area ** 2 ) * 3.1416;
	return circleArea;

}
let circleArea = printCircleArea(15);
console.log(circleArea);

function printAverage(average1,average2,average3,average4){
	console.log("The average of " + average1 + ", " + average2 + ", " + average3 + "," + " and " + average4 + ":");
	let averageVar = (average1 + average2 + average3 + average4) / 4 ;
	return averageVar;
}
let averageVar = printAverage(20,40,60,80);
console.log(averageVar);

function checkPercentage(percent1,percent2){
	console.log("Is " + percent1 + "/" + percent2 + " a passing score?");
	let totalPercentage = (percent1 / percent2) * 100;
	let isPassed = totalPercentage >= 75;
	return isPassed;

};
let isPassed = checkPercentage(38,50);
let isPassingScore = isPassed;
console.log(isPassingScore);